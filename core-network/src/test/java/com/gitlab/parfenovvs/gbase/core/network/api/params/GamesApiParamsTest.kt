package com.gitlab.parfenovvs.gbase.core.network.api.params

import com.google.common.truth.Truth.assertThat
import org.junit.Test

class GamesApiParamsTest {

  @Test
  fun toMap_ExistingParams_ReturnsMapWithEntities() {
    val params = GamesApiParams(dates = "dates", ordering = "ordering")
    val map = params.toMap()
    assertThat(map).containsExactly(
      GamesApiParams.KEY_DATES, params.dates,
      GamesApiParams.KEY_ORDERING, params.ordering,
    )
  }

  @Test
  fun toMap_NullParams_ReturnsEmptyMap() {
    val params = GamesApiParams()
    val map = params.toMap()
    assertThat(map).isEmpty()
  }
}