package com.gitlab.parfenovvs.gbase.core.network.api

import com.gitlab.parfenovvs.gbase.core.network.model.GameDetailsDto
import com.gitlab.parfenovvs.gbase.core.network.model.ImageDto
import com.gitlab.parfenovvs.gbase.core.network.model.ScreenshotsResponse
import com.google.common.truth.Truth.assertThat
import io.mockk.coEvery
import io.mockk.mockkClass
import io.mockk.unmockkAll
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before

import org.junit.Test
import java.io.IOException

class GameDetailsRemoteDataSourceImplTest {

  private lateinit var mockApi: RawgApi
  private lateinit var gameDetailsRemoteDataSource: GameDetailsRemoteDataSource

  @Before
  fun setUp() {
    mockApi = mockkClass(RawgApi::class)
    gameDetailsRemoteDataSource = GameDetailsRemoteDataSourceImpl(mockApi)
  }

  @After
  fun tearDown() {
    unmockkAll()
  }

  @Test
  fun gameDetails_Success_ReturnsDto() = runTest {
    val id = 1L
    val expectedResponse = GameDetailsDto(id = id, "", null, "")
    coEvery { mockApi.gameDetails(id) } returns expectedResponse
    val actualResponse = gameDetailsRemoteDataSource.gameDetails(id)
    assertThat(actualResponse).isEqualTo(expectedResponse)
  }

  @Test
  fun gameScreenshots_SetNumberOfScreenshots_ReturnsEqualScreenshots() = runTest {
    val id = 1L
    val number = 3
    val expectedResponse = ScreenshotsResponse(List(3) { ImageDto("url") })
    coEvery { mockApi.gameScreenshots(any(), number) } returns expectedResponse
    val actualResponse = gameDetailsRemoteDataSource.gameScreenshots(id, number)
    assertThat(actualResponse).isEqualTo(expectedResponse.results)
  }

  @Test
  fun gameScreenshots_SetNumberOfScreenshots_ReturnsLessScreenshots() = runTest {
    val id = 1L
    val number = 3
    val actualNumber = number - 1
    val expectedResponse = ScreenshotsResponse(List(actualNumber) { ImageDto("url") })
    coEvery { mockApi.gameScreenshots(any(), number) } returns expectedResponse
    val actualResponse = gameDetailsRemoteDataSource.gameScreenshots(id, number)
    assertThat(actualResponse).isEqualTo(expectedResponse.results)
  }
}