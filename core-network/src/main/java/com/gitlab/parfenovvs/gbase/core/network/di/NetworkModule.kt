package com.gitlab.parfenovvs.gbase.core.network.di

import android.content.Context
import com.gitlab.parfenovvs.gbase.core.network.BuildConfig
import com.gitlab.parfenovvs.gbase.core.network.R
import com.gitlab.parfenovvs.gbase.core.network.api.*
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class NetworkModule {

  @Binds
  abstract fun gamesRemoteDataSource(impl: GamesRemoteDataSourceImpl): GamesRemoteDataSource

  @Binds
  abstract fun gameDetailsRemoteDataSource(impl: GameDetailsRemoteDataSourceImpl): GameDetailsRemoteDataSource

  companion object {
    private const val BASE_URL = "https://api.rawg.io/"

    @Provides
    @Singleton
    fun provideApi(@ApplicationContext context: Context): RawgApi {
      val apiKey = context.getString(R.string.api_key)
      return Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(
          OkHttpClient.Builder()
            .addInterceptor { chain ->
              val originalRequest = chain.request()
              val request = originalRequest.newBuilder()
                .url(
                  originalRequest.url.newBuilder()
                    .addQueryParameter("key", apiKey)
                    .build()
                )
                .build()
              chain.proceed(request)
            }
            .addInterceptor(HttpLoggingInterceptor().apply {
              level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                else HttpLoggingInterceptor.Level.NONE
            })
            .build()
        )
        .build()
        .create(RawgApi::class.java)
    }
  }
}