package com.gitlab.parfenovvs.gbase.core.network.api

import com.gitlab.parfenovvs.gbase.core.network.model.GameDetailsDto
import com.gitlab.parfenovvs.gbase.core.network.model.ScreenshotsResponse
import com.gitlab.parfenovvs.gbase.core.network.model.base.PagedResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.QueryMap

interface RawgApi {

  @GET("/api/games")
  suspend fun games(@QueryMap params: Map<String, String>): PagedResponse

  @GET("/api/games/{id}")
  suspend fun gameDetails(@Path("id") id: Long): GameDetailsDto

  @GET("/api/games/{id}/screenshots")
  suspend fun gameScreenshots(@Path("id") id: Long, @Query("page_size") number: Int): ScreenshotsResponse
}