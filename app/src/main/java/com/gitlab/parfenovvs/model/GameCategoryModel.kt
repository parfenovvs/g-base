package com.gitlab.parfenovvs.model

import com.gitlab.parfenovvs.gbase.core.network.api.PagingState
import com.gitlab.parfenovvs.gbase.core.network.model.GameDto

data class GameCategoryModel(
  val title: String,
  val category: CategoryType,
  val dataState: PagingState<List<GameDto>>
)