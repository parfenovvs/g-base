package com.gitlab.parfenovvs.model

import com.gitlab.parfenovvs.gbase.core.network.api.params.GamesApiParams

sealed class CategoryType(
  val internalId: Int,
  val filters: GamesApiParams, //TODO move these static params as dynamic filters in the future
) {
  object MostAnticipated : CategoryType(
    0, GamesApiParams(
      dates = "2021-07-12,2024-01-01", //TODO use DateTime API for the filters
      ordering = "-added"
    )
  )

  object LatestReleases : CategoryType(
    1, GamesApiParams(
      dates = "2021-01-01,2021-07-12",
      ordering = "-released"
    )
  )

  object Rated : CategoryType(
    2, GamesApiParams(
      dates = "2021-01-01,2021-07-12",
      ordering = "-rated"
    )
  )
}