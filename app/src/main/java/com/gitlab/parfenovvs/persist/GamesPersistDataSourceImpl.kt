package com.gitlab.parfenovvs.persist

import com.gitlab.parfenovvs.gbase.core.network.model.GameDto
import com.gitlab.parfenovvs.persist.model.GameEntity
import com.gitlab.parfenovvs.model.CategoryType
import javax.inject.Inject

class GamesPersistDataSourceImpl @Inject constructor(
  private val gamesDao: GamesDao,
) : GamesPersistDataSource {

  override fun games(categoryType: CategoryType): List<GameDto> = when (categoryType) {
    is CategoryType.MostAnticipated -> gamesDao.getMostAnticipated()
    is CategoryType.LatestReleases -> gamesDao.getLatestReleases()
    is CategoryType.Rated -> gamesDao.getRated()
  }.map { entity -> entity.toDto() }

  override fun insert(games: List<GameDto>, categoryType: CategoryType) {
    gamesDao.insertAll(
      games.map { it.toEntity(categoryType.internalId) }
    )
  }

  override fun clear(categoryType: CategoryType) {
    gamesDao.clear(categoryType.internalId)
  }

  private fun GameEntity.toDto() = GameDto(
    id = id,
    title = title,
    image = image,
    released = released,
    added = added,
    rating = rating,
  )

  private fun GameDto.toEntity(categoryId: Int) = GameEntity(
    id = id,
    title = title,
    image = image,
    released = released,
    added = added,
    rating = rating,
    categoryId = categoryId,
  )
}