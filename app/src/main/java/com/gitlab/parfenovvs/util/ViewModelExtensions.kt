package com.gitlab.parfenovvs.util

import androidx.lifecycle.SavedStateHandle

fun <T> SavedStateHandle.require(key: String): T =
  (get(key) as? T) ?: throw IllegalStateException("No required state found for key: $key")