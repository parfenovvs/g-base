package com.gitlab.parfenovvs.interactor.main

import com.gitlab.parfenovvs.ui.base.model.base.ListItem
import com.gitlab.parfenovvs.model.CategoryType
import kotlinx.coroutines.flow.Flow

interface MainScreenInteractor {

  fun data(): Flow<List<ListItem>>

  suspend fun initCategory(category: CategoryType)

  suspend fun tryToLoadMore(category: CategoryType, index: Int)

  suspend fun refresh(category: CategoryType?)
}