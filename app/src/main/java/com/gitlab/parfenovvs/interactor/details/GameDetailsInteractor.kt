package com.gitlab.parfenovvs.interactor.details

import com.gitlab.parfenovvs.viewmodel.details.GameDetailsScreenModel
import kotlinx.coroutines.flow.Flow

interface GameDetailsInteractor {

  suspend fun data(): GameDetailsScreenModel

}