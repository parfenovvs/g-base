package com.gitlab.parfenovvs.ui.main

import android.os.Parcelable
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.core.view.isVisible
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.gitlab.parfenovvs.R
import com.gitlab.parfenovvs.databinding.*
import com.gitlab.parfenovvs.model.CategoryType
import com.gitlab.parfenovvs.ui.base.model.base.ListItem
import com.gitlab.parfenovvs.ui.base.model.game.*
import com.gitlab.parfenovvs.util.onClick
import com.gitlab.parfenovvs.util.release
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding


object MainScreenDelegates {

  fun mainErrorDelegate(onRefreshClick: (CategoryType?) -> Unit) =
    adapterDelegateViewBinding<MainScreenErrorItem, ListItem, ItemMainScreenErrorBinding>(
      { inflater, container -> ItemMainScreenErrorBinding.inflate(inflater, container, false) }
    ) {
      bind {
        binding.root.onClick { onRefreshClick.invoke(null) }
      }
    }

  fun gamesHorizontalDelegate(
    glide: RequestManager,
    scrollStates: MutableMap<Int, Parcelable>,
    onItemBind: (GamesHorizontalItem) -> Unit,
    onReadyToLoadMore: (CategoryType, Int) -> Unit,
    onGameClick: (GameItem) -> Unit,
    onRefreshClick: (CategoryType) -> Unit,
  ) =
    adapterDelegateViewBinding<GamesHorizontalItem, ListItem, ItemGamesHorizontalBinding>(
      { inflater, container -> ItemGamesHorizontalBinding.inflate(inflater, container, false) }
    ) {
      val adapter = GameCardsAdapter(
        glide = glide,
        onReadyToLoadMore = { pos -> onReadyToLoadMore.invoke(item.category, pos) },
        onGameClick = onGameClick,
        onRefreshClick = onRefreshClick,
      )
      binding.recyclerView.adapter = adapter

      bind {
        onItemBind.invoke(item)
        binding.titleTextView.text = item.title
        adapter.items = item.games
        scrollStates[adapterPosition]?.let {
          binding.recyclerView.layoutManager?.onRestoreInstanceState(it)
          scrollStates.remove(adapterPosition)
        }
      }

      onViewRecycled {
        binding.recyclerView.layoutManager?.onSaveInstanceState()?.let {
          scrollStates[adapterPosition] = it
        }
      }
    }

  fun wideProgressDelegate() =
    adapterDelegateViewBinding<ProgressWideItem, ListItem, ItemProgressWideBinding>(
      { inflater, container -> ItemProgressWideBinding.inflate(inflater, container, false) }
    ) {
      val animation = AnimationUtils.loadAnimation(binding.root.context, R.anim.progress_fade_out)
      binding.root.startAnimation(animation)
    }

  fun wideErrorDelegate(onClick: (CategoryType) -> Unit) =
    adapterDelegateViewBinding<ErrorWideItem, ListItem, ItemErrorWideBinding>(
      { inflater, container -> ItemErrorWideBinding.inflate(inflater, container, false) }
    ) {
      bind {
        binding.root.onClick { onClick.invoke(item.categoryType) }
      }
    }

  fun wideGameDelegate(
    glide: RequestManager,
    onReadyToLoadMore: (Int) -> Unit,
    onGameClick: (GameItem) -> Unit
  ) =
    adapterDelegateViewBinding<GameWideItem, ListItem, ItemGameWideBinding>(
      { inflater, container -> ItemGameWideBinding.inflate(inflater, container, false) }
    ) {
      bind {
        val resources = binding.root.resources
        glide.load(item.image)
          .override(
            resources.getDimensionPixelOffset(R.dimen.game_card_wide_width),
            resources.getDimensionPixelOffset(R.dimen.game_card_wide_height)
          )
          .transform(
            CenterCrop(),
            RoundedCorners(resources.getDimensionPixelOffset(R.dimen.game_card_radius))
          )
          .transition(withCrossFade())
          .placeholder(R.drawable.bg_item_placeholder)
          .into(binding.imageView)
        binding.titleTextView.text = item.title

        onReadyToLoadMore.invoke(adapterPosition)

        binding.root.onClick { onGameClick.invoke(item) }
      }

      onViewRecycled { glide.release(binding.imageView) }
    }

  fun thinProgressDelegate() =
    adapterDelegateViewBinding<ProgressThinItem, ListItem, ItemProgressThinBinding>(
      { inflater, container -> ItemProgressThinBinding.inflate(inflater, container, false) }
    ) {
      val animation = AnimationUtils.loadAnimation(binding.root.context, R.anim.progress_fade_out)
      binding.root.startAnimation(animation)
    }

  fun thinErrorDelegate(onClick: (CategoryType) -> Unit) =
    adapterDelegateViewBinding<ErrorThinItem, ListItem, ItemErrorThinBinding>(
      { inflater, container -> ItemErrorThinBinding.inflate(inflater, container, false) }
    ) {
      bind {
        binding.root.onClick { onClick.invoke(item.categoryType) }
      }
    }

  fun thinGameDelegate(
    glide: RequestManager,
    onReadyToLoadMore: (Int) -> Unit,
    onGameClick: (GameItem) -> Unit
  ) =
    adapterDelegateViewBinding<GameThinItem, ListItem, ItemGameThinBinding>(
      { inflater, container -> ItemGameThinBinding.inflate(inflater, container, false) }
    ) {
      bind {
        val resources = binding.root.resources
        glide.load(item.image)
          .override(
            resources.getDimensionPixelOffset(R.dimen.game_card_thin_width),
            resources.getDimensionPixelOffset(R.dimen.game_card_thin_height)
          )
          .transform(
            CenterCrop(),
            RoundedCorners(resources.getDimensionPixelOffset(R.dimen.game_card_radius))
          )
          .transition(withCrossFade())
          .placeholder(R.drawable.bg_item_placeholder)
          .into(binding.imageView)
        binding.titleTextView.text = item.title

        onReadyToLoadMore.invoke(adapterPosition)

        binding.root.onClick { onGameClick.invoke(item) }
      }

      onViewRecycled { glide.release(binding.imageView) }
    }
}