package com.gitlab.parfenovvs.ui.base

import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import com.gitlab.parfenovvs.R
import com.gitlab.parfenovvs.util.launchAndRepeatOnStart
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.sample
import kotlinx.coroutines.launch

abstract class BaseFragment(@LayoutRes layoutId: Int) : Fragment(layoutId) {

  fun <T> Flow<T>.collectWhileStarted(block: (T) -> Unit) {
    launchAndRepeatOnStart {
      collect { block.invoke(it) }
    }
  }

  fun Flow<Throwable>.connectErrorData() {
    lifecycleScope.launch {
      sample(1000L).collect {
        if (lifecycle.currentState >= Lifecycle.State.STARTED) {
          Toast.makeText(context, R.string.default_error, Toast.LENGTH_SHORT).show()
        }
      }
    }
  }
}