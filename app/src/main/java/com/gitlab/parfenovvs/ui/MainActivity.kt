package com.gitlab.parfenovvs.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.gitlab.parfenovvs.R
import com.gitlab.parfenovvs.databinding.ActivityMainBinding
import com.gitlab.parfenovvs.ui.main.MainFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    val binding = ActivityMainBinding.inflate(layoutInflater)
    setContentView(binding.root)
  }
}
