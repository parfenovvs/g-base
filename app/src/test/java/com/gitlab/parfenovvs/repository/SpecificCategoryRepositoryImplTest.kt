package com.gitlab.parfenovvs.repository

import com.gitlab.parfenovvs.gbase.core.network.api.GamesRemoteDataSource
import com.gitlab.parfenovvs.gbase.core.network.api.PagingState
import com.gitlab.parfenovvs.gbase.core.network.model.GameDto
import com.gitlab.parfenovvs.model.CategoryType
import com.gitlab.parfenovvs.model.GameCategoryModel
import com.gitlab.parfenovvs.persist.GamesPersistDataSource
import com.google.common.truth.Truth.assertThat
import io.mockk.*
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest

import org.junit.After
import org.junit.Before
import org.junit.Test

class SpecificCategoryRepositoryImplTest {

  private val categoryTitle = "category"
  private val categoryType = CategoryType.MostAnticipated

  private lateinit var mockGamesRemoteDataSource: GamesRemoteDataSource
  private lateinit var mockGamesPersistDataSource: GamesPersistDataSource
  private lateinit var gameCategoryRepository: GameCategoryRepository

  @Before
  fun setUp() {
    mockGamesRemoteDataSource = mockkClass(GamesRemoteDataSource::class)
    mockGamesPersistDataSource = mockkClass(GamesPersistDataSource::class, relaxed = true)
    gameCategoryRepository = SpecificCategoryRepositoryImpl(
      mockGamesRemoteDataSource,
      mockGamesPersistDataSource,
      categoryTitle,
      categoryType,
    )
  }

  @After
  fun tearDown() {
    unmockkAll()
  }

  @Test
  fun init_DoNothing_CheckInitialState() = runTest {
    assertThat(gameCategoryRepository.data().first())
      .isEqualTo(
        GameCategoryModel(
          title = categoryTitle,
          category = categoryType,
          dataState = PagingState.Initial,
        )
      )
  }

  @Test
  fun init_LoadGames_InitializesStateWithContent() = runTest {
    val games = List(3) { GameDto(it.toLong(), "", "", "", 0L, 0f) }

    coEvery { mockGamesRemoteDataSource.initialLoading(any()) } returns games

    gameCategoryRepository.init()

    assertThat(gameCategoryRepository.data().first())
      .isEqualTo(
        GameCategoryModel(
          title = categoryTitle,
          category = categoryType,
          dataState = PagingState.Content(games),
        )
      )
  }

  @Test(expected = Exception::class)
  fun init_LoadingError_InitializesError() = runTest {
    val e = Exception()

    coEvery { mockGamesRemoteDataSource.initialLoading(any()) } throws e
    coEvery { mockGamesPersistDataSource.games(any()) } returns emptyList()

    gameCategoryRepository.init()

    assertThat(gameCategoryRepository.data().first())
      .isEqualTo(
        GameCategoryModel(
          title = categoryTitle,
          category = categoryType,
          dataState = PagingState.Error(e),
        )
      )
  }

  @Test(expected = Exception::class)
  fun init_LoadingError_InitializesPersist() = runTest {
    val games = listOf(mockkClass(GameDto::class))

    coEvery { mockGamesRemoteDataSource.initialLoading(any()) } throws Exception()
    coEvery { mockGamesPersistDataSource.games(any()) } returns games

    gameCategoryRepository.init()

    assertThat(gameCategoryRepository.data().first())
      .isEqualTo(
        GameCategoryModel(
          title = categoryTitle,
          category = categoryType,
          dataState = PagingState.Persist(games),
        )
      )
  }

  @Test
  fun tryToLoadMore_LoadMore_UpdateContent() = runTest {
    val games = List(3) { GameDto(it.toLong(), "", "", "", 0L, 0f) }
    val moreGames = List(2) { GameDto(it.toLong() * 10, "", "", "", 0L, 0f) }

    coEvery { mockGamesRemoteDataSource.initialLoading(any()) } returns games
    gameCategoryRepository.init()
    coEvery { mockGamesRemoteDataSource.loadMore() } returns moreGames

    gameCategoryRepository.tryToLoadMore(games.size - 1)

    assertThat(gameCategoryRepository.data().first())
      .isEqualTo(
        GameCategoryModel(
          title = categoryTitle,
          category = categoryType,
          dataState = PagingState.Content(games.plus(moreGames))
        )
      )
  }

  @Test
  fun tryToLoadMore_WrongIndex_SameContent() = runTest {
    val games = List(3) { GameDto(it.toLong(), "", "", "", 0L, 0f) }

    coEvery { mockGamesRemoteDataSource.initialLoading(any()) } returns games
    gameCategoryRepository.init()

    gameCategoryRepository.tryToLoadMore(-1)

    assertThat(gameCategoryRepository.data().first())
      .isEqualTo(
        GameCategoryModel(
          title = categoryTitle,
          category = categoryType,
          dataState = PagingState.Content(games)
        )
      )
  }

  @Test(expected = Exception::class)
  fun tryToLoadMore_LoadingError_InitializeError() = runTest {
    val games = List(3) { GameDto(it.toLong(), "", "", "", 0L, 0f) }
    val e = Exception()

    coEvery { mockGamesRemoteDataSource.initialLoading(any()) } returns games
    gameCategoryRepository.init()
    coEvery { mockGamesRemoteDataSource.loadMore() } throws e

    gameCategoryRepository.tryToLoadMore(games.size - 1)

    assertThat(gameCategoryRepository.data().first())
      .isEqualTo(
        GameCategoryModel(
          title = categoryTitle,
          category = categoryType,
          dataState = PagingState.Error(e)
        )
      )
  }

  @Test(expected = Exception::class)
  fun tryToLoadMore_LoadingError_InitializePersist() = runTest {
    val games = List(3) { GameDto(it.toLong(), "", "", "", 0L, 0f) }
    val persistGames = listOf(mockkClass(GameDto::class))
    val e = Exception()

    coEvery { mockGamesRemoteDataSource.initialLoading(any()) } returns games
    gameCategoryRepository.init()
    coEvery { mockGamesRemoteDataSource.loadMore() } throws e
    coEvery { mockGamesPersistDataSource.games(any()) } returns persistGames

    gameCategoryRepository.tryToLoadMore(games.size - 1)

    assertThat(gameCategoryRepository.data().first())
      .isEqualTo(
        GameCategoryModel(
          title = categoryTitle,
          category = categoryType,
          dataState = PagingState.Persist(persistGames)
        )
      )
  }

  @Test
  fun refresh_Force_InitializeContent() = runTest {
    val games = List(3) { GameDto(it.toLong(), "", "", "", 0L, 0f) }

    coEvery { mockGamesRemoteDataSource.initialLoading(any()) } returns games

    gameCategoryRepository.refresh(true)

    assertThat(gameCategoryRepository.data().first())
      .isEqualTo(
        GameCategoryModel(
          title = categoryTitle,
          category = categoryType,
          dataState = PagingState.Content(games),
        )
      )
  }

  @Test(expected = Exception::class)
  fun refresh_ErrorState_InitializeContent() = runTest {
    val e = Exception()

    coEvery { mockGamesRemoteDataSource.initialLoading(any()) } throws e
    coEvery { mockGamesPersistDataSource.games(any()) } returns emptyList()

    gameCategoryRepository.init()

    gameCategoryRepository.refresh(false)

    assertThat(gameCategoryRepository.data().first())
      .isEqualTo(
        GameCategoryModel(
          title = categoryTitle,
          category = categoryType,
          dataState = PagingState.Error(e),
        )
      )
  }

  @Test
  fun refresh_ContentState_LoadMore() = runTest {
    val games = List(3) { GameDto(it.toLong(), "", "", "", 0L, 0f) }
    val moreGames = List(2) { GameDto(it.toLong() * 10, "", "", "", 0L, 0f) }

    coEvery { mockGamesRemoteDataSource.initialLoading(any()) } returns games
    gameCategoryRepository.init()
    coEvery { mockGamesRemoteDataSource.loadMore() } returns moreGames
    coEvery { mockGamesRemoteDataSource.updateParams(any(), any()) } just Runs

    gameCategoryRepository.refresh(false)

    assertThat(gameCategoryRepository.data().first())
      .isEqualTo(
        GameCategoryModel(
          title = categoryTitle,
          category = categoryType,
          dataState = PagingState.Content(games.plus(moreGames))
        )
      )
  }
}